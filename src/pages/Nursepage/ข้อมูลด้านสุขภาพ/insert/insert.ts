import { Component } from "@angular/core";
import {
  IonicPage,
  NavController,
  NavParams,
  AlertController,
} from "ionic-angular";
import { GlobalProvider } from "../../../../providers/global/global";
import { Http, Headers, RequestOptions } from "@angular/http";
import moment from "moment";
import {
  FormGroup,
  FormBuilder,
  Validators,
  RequiredValidator,
} from "@angular/forms";
import "moment/locale/TH";

@IonicPage()
@Component({
  selector: "page-insert",
  templateUrl: "insert.html",
})
export class InsertPage {
  formgroup: FormGroup;
  showCompOther: boolean = false;
  showstatusother: boolean = false;
  showAntiOther: boolean = false;
  showBlockOther: boolean = false;
  showAntiPill: boolean = true;
  showBlockPill: boolean = true;
  showAllergy: boolean = true;
  showMajoradverse: boolean = true;
  showAllergyOther: boolean = false;
  toxics = [
    {
      name: "ไม่มี",
    },
    {
      name: "thyroid crisis",
    },
    {
      name: "thyrotoxic myopathy",
    },
    {
      name: "heart arrhythmia",
    },
    {
      name: "heart failure"
    },
    {
      name: "อื่นๆ",
    }
  ];
  allergy = [
    {
      name: "drug allergy",
    },
    {
      name: "jaundice",
    },
    {
      name: "agranulocytosis",
    },
    {
      name: "อื่นๆ",
    }
  ];

  startMin: any;
  startMax: any;
  pages;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public formBuilder: FormBuilder,
    public global: GlobalProvider,
    public http: Http,
    public alertCtrl: AlertController
  ) {
    this.startMin = moment().add(443, "y").format("YYYY");
    this.startMax = moment().add(543, "y").format("YYYY");
    this.pages = new Array(100);
    let year = this.startMax
    for (let index = 0; index <= 100; index++) {
      this.pages[index] = year;
      year--;
    }

    this.formgroup = formBuilder.group({
      month: [navParams.get("month") || moment().format("MMMM"),],
      year: [navParams.get("year") || this.startMax,],
      Toxic: ["",],
      ToxicEtc: ["",],
      comp_name: [navParams.get("comp_name")],
      comp_majoradverse: [navParams.get("comp_majoradverse")],
      Allergy: ["", Validators.required],
      Drug: ["",],
      DrugEtc: ["",],
      comp_description: [navParams.get("comp_description")]
    });
  }

  ionViewDidLoad() {
    console.log("ionViewDidLoad InsertPage");

    this.setAllery()
    this.setToxic()
  }

  showComp(txt) {
    if (txt === "อื่นๆ") {
      this.showCompOther = true;
      this.showstatusother = false;
      this.formgroup.controls.comp_name.setValue(null)
    } else if (txt === "heart arrhythmia") {
      this.showstatusother = true;
      this.showCompOther = false;
    } else {
      this.formgroup.controls.comp_name.setValue(null)
      this.showstatusother = false;
      this.showCompOther = false;
    }
    console.log(txt)
  }

  sendAllery() {
    let allergy: string = this.formgroup.controls.Allergy.value
    if (allergy === "ระบุอาการแพ้ยา") {
      let drug: string = this.formgroup.controls.Drug.value
      if (drug === "อื่นๆ") {
        return this.formgroup.controls.DrugEtc.value
      } else {
        return this.formgroup.controls.Drug.value
      }
    } else {
      return this.formgroup.controls.Allergy.value
    }
  }
  sendToxic() {
    let toxic: string = this.formgroup.controls.Toxic.value
    if (toxic === "อื่นๆ") {
      return this.formgroup.controls.ToxicEtc.value
    } else {
      return this.formgroup.controls.Toxic.value
    }
  }

  setToxic() {
    let toxic
    if (this.navParams.get("comp_status")) {
      toxic = this.navParams.get("comp_status")
    } else {
      toxic = "ไม่มี"
    }

    if (this.toxics.find(names => names.name === toxic)) {
      this.formgroup.controls.Toxic.setValue(toxic)

    } else {
      this.formgroup.controls.Toxic.setValue("อื่นๆ")
      this.formgroup.controls.ToxicEtc.setValue(toxic)
    }
    this.showComp(this.formgroup.controls.Toxic.value)
  }

  setAllery() {
    let allery
    if (this.navParams.get("comp_indication")) {
      allery = this.navParams.get("comp_indication")
    } else {
      allery = "medical failure"
    }

    if (allery != "medical failure" && allery != "มีภาวะแทรกซ้อนของโรค" && allery != "Major adverse reactions to") {
      this.formgroup.controls.Allergy.setValue("ระบุอาการแพ้ยา")
      this.formgroup.controls.Drug.setValue(allery)
      this.allergy.unshift({
        name: allery
      })
    } else {
      this.formgroup.controls.Allergy.setValue(allery)
      this.formgroup.controls.Drug.setValue("drug allergy")
    }
    this.addAllery(this.formgroup.controls.Allergy.value)
    this.addDrug(this.formgroup.controls.Drug.value)
  }

  addAllery(txt) {
    if (txt == "ระบุอาการแพ้ยา") {
      this.showAllergy = true
      this.showMajoradverse = false;
      this.formgroup.controls.comp_majoradverse.setValue(null)
      this.formgroup.controls.comp_description.setValue(null)
    } else if (txt == "Major adverse reactions to") {
      this.showAllergy = false
      this.showMajoradverse = true;
    } else {
      this.formgroup.controls.comp_majoradverse.setValue(null)
      this.formgroup.controls.comp_description.setValue(null)
      this.showAllergy = false;
      this.showMajoradverse = false;
    }
  }

  addDrug(txt) {
    if (txt == "อื่นๆ") {
      this.showAllergyOther = true
    } else {
      this.showAllergyOther = false
      this.formgroup.controls.DrugEtc.setValue(null)
    }
  }

  update() {
    let toxicother: string = this.formgroup.controls.ToxicEtc.value
    let toxic: string = this.formgroup.controls.Toxic.value
    let drugother: string = this.formgroup.controls.DrugEtc.value
    let drug: string = this.formgroup.controls.Drug.value
    if (!toxicother && toxic === "อื่นๆ") {
      this.presentAlert("กรุณาระบุอาการผิดปกติ")
    }

    else if (!drugother && drug === "อื่นๆ") {
      this.presentAlert("กรุณาระบุอาการแพ้ยา")
    } else {
      let body = {
        idcard: this.global.getpatientID(),
        round: 1,

        month: this.formgroup.controls.month.value,
        year: this.formgroup.controls.year.value,
        comp_status: this.sendToxic(),
        comp_name: this.formgroup.controls.comp_name.value,
        comp_indication: this.sendAllery(),
        comp_majoradverse: this.formgroup.controls.comp_majoradverse.value,
        comp_description: this.formgroup.controls.comp_description.value

      }
      this.navCtrl.getPrevious().data.formData = body
      let headers = new Headers({ "Content-type": "application/json" });
      let options = new RequestOptions({ headers: headers });
      this.http
        .post(
          this.global.getIP() + "/healthdata.php?method=update_complication&role=" + this.global.getSelectRole(),
          body,
          options
        )
        .map(res => res.json())
        .subscribe(
          data => {
            if (data.result) {
              this.presentAlert(data.result);
              this.navCtrl.pop()
            }
          },
          error => {
            console.log(error);
          }
        );

    }
  }
  async presentAlert(txt: string) {
    let alert = await this.alertCtrl.create({
      title: 'การแจ้งเตือน',
      subTitle: txt,
      buttons: ['Ok']
    });
    alert.present();
  }

  async presentConfirm() {
    let alert = await this.alertCtrl.create({
      title: "ยืนยันการแก้ไขข้อมูล",
      message: "",
      buttons: [
        {
          text: "ยกเลิก",
          role: "cancel",
          handler: () => {
            console.log("Cancel clicked");
          }
        },
        {
          text: "ยืนยัน",
          handler: () => {
            this.update();
          }
        }
      ]
    });
    alert.present();
  }

}
