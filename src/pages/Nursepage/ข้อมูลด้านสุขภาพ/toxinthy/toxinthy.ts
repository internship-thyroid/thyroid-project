import { Component } from "@angular/core";
import { IonicPage, NavController, NavParams } from "ionic-angular";
import { InsertPage } from "../insert/insert";
import { GlobalProvider } from "../../../../providers/global/global";
import { Http, Headers, RequestOptions } from "@angular/http";
import moment from "moment";
import "moment/locale/TH";
import { CompileMetadataResolver } from "@angular/compiler";

/**
 * Generated class for the ToxinthyPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: "page-toxinthy",
  templateUrl: "toxinthy.html",
})
export class ToxinthyPage {
  month: string;
  year: string;
  comp_status: string;
  comp_name: string;
  comp_indication: string;
  comp_majoradverse: string;
  comp_description: string;

  monShow;
  yearShow;

  toxic: string;
  allergy: string;
  allergyOther: string;
  otherStatus = false;
  otherAllergy = false;
  showData: boolean = true;
  showButtonedit: boolean = this.checkRole(this.global.getSelectRole());
  toxics = [
    {
      name: "ไม่มี",
    },
    {
      name: "thyroid crisis",
    },
    {
      name: "thyrotoxic myopathy",
    },
    {
      name: "heart arrhythmia",
    },
    {
      name: "heart failure",
    },
  ];
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public global: GlobalProvider,
    public http: Http
  ) { }

  ionViewDidLoad() {
    console.log("ionViewDidLoad ToxinthyPage");
    this.getdata();

  }
  checkRole(role) {
    if (role === "หมอ") {
      return true;
    } else {
      return false;
    }
  }

  ionViewWillEnter() {
    this.getdata();
  }
  getdata() {
    let headers = new Headers({ "Content-type": "application/json" });
    let options = new RequestOptions({ headers: headers });
    let body = JSON.stringify({
      idcard: this.global.getpatientID(),
      round: 1,
    });
    console.log("body : " + body);
    this.http
      .post(
        this.global.getIP() +
        "/healthdata.php?method=get_complication&role=" +
        this.global.getSelectRole(),
        body,
        options
      )
      .map((res) => res.json())
      .subscribe(
        (data) => {
          if (data.month) {
            this.month = data.month;
            this.monShow = moment(data.month, "MMMM").fromNow(true);
            this.year = data.year;
            this.yearShow = moment(data.year, "YYYY")
              .subtract(543, "y")
              .fromNow(true);
            this.comp_status = data.comp_status;
            this.comp_name = data.comp_name;
            this.comp_indication = data.comp_indication;
            this.comp_majoradverse = data.comp_majoradverse;
            this.comp_description = data.comp_description;
            console.log(data);
            this.toxic = this.comp_status;
            this.setAllergy();
          } else {
            this.showData = false;
            console.log("data : " + this.showData);
          }
        },
        (error) => {
          console.log(error);
        }
      );
  }

  setAllergy() {
    let allergy = this.comp_indication;
    if (
      allergy != "medical failure" &&
      allergy != "มีภาวะแทรกซ้อนของโรค" &&
      allergy != "Major adverse reactions to" &&
      allergy != "drug allergy" &&
      allergy != "jaundice" &&
      allergy != "agranulocytosis"
    ) {
      this.allergy = "อาการแพ้ยา";
      this.allergyOther = allergy;
    } else {
      this.allergy = allergy;
      this.allergyOther = "";
    }
  }

  insert() {
    this.navCtrl.push(InsertPage, {
      month: this.month,
      year: this.year,
      comp_status: this.comp_status,
      comp_name: this.comp_name,
      comp_indication: this.comp_indication,
      comp_majoradverse: this.comp_majoradverse,
      comp_description: this.comp_description,
    });
  }
}
