import { Component } from "@angular/core";
import { IonicPage, NavController, NavParams } from "ionic-angular";
import { RadiographicPage } from "../radiographic/radiographic";
import { ShowExaminationPage } from "../show-examination/show-examination";
import { EditconditionultrasoundPage } from "../editconditionultrasound/editconditionultrasound";
import { GlobalProvider } from "../../../../providers/global/global";
import { RequestOptions } from "@angular/http";
import * as moment from "moment";
import { Headers } from "@angular/http";
import { Http } from "@angular/http";

/**
 * Generated class for the ConditionUltrasoundPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: "page-condition-ultrasound",
  templateUrl: "condition-ultrasound.html",
})
export class ConditionUltrasoundPage {
  momentjs: any = moment;
  Thy_ult_data: any = '';
  // thyroid_image: any;
  // thy_ult_result: any;
  thy_che_date: any;
  thy_che_size: any;
  thy_che_found: any;
  items: any
  showMenu: boolean;
  showData: boolean = true;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public global: GlobalProvider,
    public http: Http
  ) {
    if (this.global.getSelectRole() === "หมอ") {
      this.showMenu = true;
    } else {
      this.showMenu = false;
    }
  }

  async ionViewDidLoad() {
    console.log('ionViewDidLoad Condition-UltrasoundPage');
    // this.getThyroidUltrasound();
  }

  ionViewWillEnter() {
    this.getThyroidUltrasound();
  }

  async getThyroidUltrasound() { //ดึงข้อมูลจาก Thyroid Ultrasound มาแสดง
    let headers = new Headers({ "Content-type": "application/json" });
    let options = new RequestOptions({ headers: headers });
    let body = JSON.stringify({
      idcard: this.global.getpatientID(),
      round: this.global.getSelectRound()
    });
    console.log("body : " + body);
    await this.http.post(
      this.global.getIP() + "/result.php?method=get_thyroidUltraPic&role=" + this.global.getSelectRole(),
      body,
      options
    )
      .map(res => res.json())
      .subscribe(
        data => {
          if (data.thy_che_size != null) {
            this.showData = true;
            this.thy_che_date = moment(
              data.thy_che_date,
              "YYYY-MM-DD"
            ).format("Do MMMM YYYY");
            this.thy_che_size = data.thy_che_size;
            this.thy_che_found = data.thy_che_found;
            console.log(JSON.stringify(data));
          } else {
            this.showData = false;
          }
        },
        error => {
          console.log(error);
        }
      );
  }

  found() {
    this.navCtrl.push(ShowExaminationPage);
  }
  editDetail() {
    this.navCtrl.push(EditconditionultrasoundPage, {
      thy_che_date: this.thy_che_date,
      thy_che_size: this.thy_che_size,
      thy_che_found: this.thy_che_found,
    });
  }
}
