import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ConditionUltrasoundPage } from './condition-ultrasound';

@NgModule({
  declarations: [
    ConditionUltrasoundPage,
  ],
  imports: [
    IonicPageModule.forChild(ConditionUltrasoundPage),
  ],
})
export class ConditionUltrasoundPageModule {}
