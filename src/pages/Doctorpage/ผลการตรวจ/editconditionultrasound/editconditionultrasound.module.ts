import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { EditconditionultrasoundPage } from './editconditionultrasound';

@NgModule({
  declarations: [
    EditconditionultrasoundPage,
  ],
  imports: [
    IonicPageModule.forChild(EditconditionultrasoundPage),
  ],
})
export class EditconditionultrasoundPageModule {}
